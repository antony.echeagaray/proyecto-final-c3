import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.0/firebase-app.js";
import { getDatabase, ref, push, onValue, set, update, get } from "https://www.gstatic.com/firebasejs/10.5.0/firebase-database.js";
import { getStorage as getFirebaseStorage, ref as storageRef, uploadBytesResumable, getDownloadURL } from "https://www.gstatic.com/firebasejs/10.5.0/firebase-storage.js";



// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCqfDwCSkJONQuV_lVNqdo46eavVCK0lDc",
    authDomain: "proyecto-final-c3-tienda.firebaseapp.com",
    projectId: "proyecto-final-c3-tienda",
    storageBucket: "proyecto-final-c3-tienda.appspot.com",
    messagingSenderId: "834084720593",
    appId: "1:834084720593:web:ba200d4d639a23e20c9af0"
};

const app = initializeApp(firebaseConfig); // Configuración de Firebase
const db = getDatabase(app);


//Mostrar los productos


function displayProducts() {
    const productsRef = ref(db, 'products/');
    onValue(productsRef, (snapshot) => {
        const data = snapshot.val();
        const productsList = document.getElementById('productsList');
        productsList.innerHTML = ''; // Limpiar la lista

        for(let id in data) {
            const product = data[id];
            const statusDisplay = product.active === undefined ? 'null' : (product.active ? 'active' : 'inactive');
            let productItem = `
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    <img src="${product.image}" alt="${product.name}" width="50" height="50" class="mr-3 rounded">
                    <div>
                        ${product.name} - ${product.price} - ${statusDisplay} -${product.category}
                    </div>
                    <div class="button-options">
                        <button data-id="${id}" class="btn btn-warning btn-sm ml-2 edit-btn">Editar</button>
                        <button data-id="${id}" class="btn btn-danger btn-sm ml-2 delete-btn">Eliminar</button>
                    </div>
                </li>`;
            productsList.innerHTML += productItem;
        }
        
        // Event listeners para los botones de eliminar y editar
        document.querySelectorAll('.delete-btn').forEach(btn => {
            btn.addEventListener('click', deleteProduct);
        });
        
        document.querySelectorAll('.edit-btn').forEach(btn => {
            btn.addEventListener('click', editProduct);
        });
    });
}

//Mostrar las categorias

function displayCategories() {
    const categoriesRef = ref(db, 'categories/');
    onValue(categoriesRef, (snapshot) => {
        const data = snapshot.val();
        const categoriesList = document.getElementById('categoriesList');
        categoriesList.innerHTML = ''; // Limpiar la lista

        for(let id in data) {
            const category = data[id];
            let categoryItem = `
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    <div>
                        ${category.name}
                    </div>
                    <div class="button-options">
                        <button data-id="${id}" class="btn btn-warning btn-sm ml-2 edit-btn">Editar</button>
                        <button data-id="${id}" class="btn btn-danger btn-sm ml-2 delete-btn">Eliminar</button>
                    </div>
                </li>`;
            categoriesList.innerHTML += categoryItem;
        }
        
        // Event listeners para los botones de eliminar y editar
        document.querySelectorAll('.delete-btn').forEach(btn => {
            btn.addEventListener('click', deleteCategory);
        });
        
        document.querySelectorAll('.edit-btn').forEach(btn => {
            btn.addEventListener('click', editCategory);
        });
    });
}

//Añadir las categorias existentes al select

function populateCategorySelect() {
    const categoriesRef = ref(db, 'categories/');
    onValue(categoriesRef, (snapshot) => {
        const data = snapshot.val();
        const categorySelect = document.getElementById('category');
        
        // Limpiar las opciones existentes, dejando solamente la opción por defecto
        categorySelect.innerHTML = '<option selected>Select Category</option>';
        
        for(let id in data) {
            const category = data[id];
            const option = document.createElement('option');
            option.value = category.name;
            option.innerText = category.name;
            categorySelect.appendChild(option);
        }
    });
}

// Llamar a la función para poblar el select con las categorías
populateCategorySelect();

// Llamar a la función para mostrar las categorías
displayCategories();



document.addEventListener("DOMContentLoaded", function() {
    displayProducts();
});


//Editar - Eliminar los productos
let isEditing = null;
//Eliminar
function deleteProduct(event) {
    const productId = event.target.getAttribute('data-id');
    const productRef = ref(db, 'products/' + productId);
    set(productRef, null).then(() => {
        showAlert("Producto eliminado con éxito.", "success");
    }).catch(error => {
        showAlert("Error al eliminar el producto: " + error.message, "danger");
    });
}

//Editar
function editProduct(event) {
    const productId = event.target.getAttribute('data-id');
    const productRef = ref(db, 'products/' + productId);
    onValue(productRef, (snapshot) => {
        const product = snapshot.val();
        document.getElementById('productName').value = product.name;
        document.getElementById('productExtract').value = product.extract;
        document.getElementById('productPrice').value = product.price;
        document.getElementById('productStatus').checked = product.active;
        document.getElementById('category').value = product.category;
        // Agrega la URL de la imagen a la vista previa
        if (product.image) {
            const imagePreview = document.getElementById('imagePreview') || document.createElement('img');
            imagePreview.src = product.image;
            imagePreview.id = 'imagePreview';
            imagePreview.style.maxWidth = '100%';
            imagePreview.style.height = 'auto';
            // Asegurarse de que el contenedor de la vista previa esté presente
            let previewContainer = document.getElementById('previewContainer');
            if (!previewContainer) {
                previewContainer = document.createElement('div');
                previewContainer.id = 'previewContainer';
                previewContainer.style.marginTop = '10px';
                document.getElementById('productImage').parentNode.insertBefore(previewContainer, document.getElementById('productImage').nextSibling);
            }
            // Añade la imagen de vista previa si no ha sido añadida antes
            if (!document.getElementById('imagePreview')) {
                previewContainer.appendChild(imagePreview);
            }
        }
        // Cambiar texto del botón a "Edit Product"
        document.getElementById('submitBtn').innerText = 'Edit Product';
        // Establece el atributo de edición con el ID del producto que estás editando
        document.getElementById('productForm').setAttribute('data-editing', productId);
    });
}

function updateProduct(id, productData) {
    const productRef = ref(db, 'products/' + id);
    update(productRef, productData)
        .then(() => {
            showAlert("Product successfully updated.", "success");
            populateCategorySelect();
        })
        .catch((error) => {
            showAlert("Error updating product: " + error.message, "danger");
        });
}

function addProduct(product, onSuccess, onError) {
    // 1. Crear una referencia al nodo 'products' en la base de datos.
    const productsRef = ref(db, 'products');
    
    // 2. Agregar el producto a la base de datos.
    // El método 'push' genera automáticamente un ID único para cada nuevo producto.
    push(productsRef, product)
        .then(() => {
            // 3. Si la operación de inserción fue exitosa, ejecuta la función 'onSuccess'.
            onSuccess("¡Producto agregado con Exito!");
        })
        .catch((error) => {
            // 4. Si ocurre un error durante la operación de inserción, ejecuta la función 'onError' y pasa el error.
            onError(error);
        });
}

document.getElementById('productForm').addEventListener('submit', (e) => {
    e.preventDefault();

    const name = document.getElementById('productName').value;
    const extract = document.getElementById('productExtract').value;
    const price = document.getElementById('productPrice').value;
    const isActive = document.getElementById('productStatus').checked;
    const category = document.getElementById('category').value
    const imageFile = document.getElementById('productImage').files[0]; // Obtener el archivo

    const editingId = document.getElementById('productForm').getAttribute('data-editing');

    // Si se seleccionó una imagen, súbela
    if (imageFile) {
        uploadImage(imageFile, (imageUrl) => {
            if (editingId) {
                // Modo de edición: Actualizar el producto
                updateProduct(editingId, { name, extract, price, image: imageUrl, active: isActive, category });
            } else {
                // Modo de adición: Agregar nuevo producto
                addProduct({ name, extract, price, image: imageUrl, active: isActive, category }, (message) => {
                    showAlert(message, "success");
                }, (error) => {
                    showAlert("Error al agregar el producto: " + error.message, "danger");
                });
            }
            populateCategorySelect();
            resetForm();
        });
    } else {
        if (editingId) {
            // Modo de edición sin cambiar la imagen: Actualizar el producto
            updateProduct(editingId, { name, extract, price, active: isActive, category });
        } else {
            // Aquí puedes manejar el caso en que se intenta agregar un producto sin imagen
            showAlert("Seleccione una imagen para el producto.", "warning");
            return;
        }
        populateCategorySelect();
        resetForm();
    }
    // Restablecer texto del botón y variable isEditing
    document.getElementById('submitBtn').innerText = 'Add Product';
    isEditing = null;
});

function resetForm() {
    document.getElementById('productForm').reset();
    document.getElementById('productForm').removeAttribute('data-editing');
}

document.getElementById('productImage').addEventListener('change', function(event) {
    const file = event.target.files[0];
    if (file) {
        const reader = new FileReader();
        reader.onload = function(e) {
            let imagePreview = document.getElementById('imagePreview');
            if (!imagePreview) {
                // Si no existe, crear la imagen de vista previa
                imagePreview = document.createElement('img');
                imagePreview.id = 'imagePreview';
                imagePreview.style.maxWidth = '100%'; // Ajusta al ancho del contenedor
                imagePreview.style.height = 'auto'; // Mantén la proporción de la imagen
                document.getElementById('previewContainer').appendChild(imagePreview);
            }
            // Establecer la imagen como la vista previa
            imagePreview.src = e.target.result;
        };
        reader.readAsDataURL(file);
    }
});


//CATEGORIAS

let isEditingCategory = null;

// Eliminar una categoría
function deleteCategory(event) {
    const categoryId = event.target.getAttribute('data-id');
    const categoryRef = ref(db, 'categories/' + categoryId);
    set(categoryRef, null).then(() => {
        showAlert("Categoría eliminada con éxito.", "success");
    }).catch(error => {
        showAlert("Error al eliminar la categoría: " + error.message, "danger");
    });
}

// Editar una categoría
function editCategory(event) {
    const categoryId = event.target.getAttribute('data-id');
    const categoryRef = ref(db, 'categories/' + categoryId);
    onValue(categoryRef, (snapshot) => {
        const category = snapshot.val();
        document.getElementById('categoryName').value = category.name;
        
        // Cambiar texto del botón a "Edit Category"
        document.getElementById('submitCategoryBtn').innerText = 'Edit Category';
        
        // Establece el atributo de edición con el ID de la categoría que estás editando
        document.getElementById('categoryForm').setAttribute('data-editing', categoryId);
    });
}

// Añadir una nueva categoría
function addCategory(category, onSuccess, onError) {
    const categoriesRef = ref(db, 'categories');
    push(categoriesRef, category)
        .then(() => {
            onSuccess("¡Categoría agregada con éxito!");
            populateCategorySelect();
        })
        .catch((error) => {
            onError(error);
        });
}


// Actualizar una categoría existente
function updateCategory(id, categoryData) {
    const categoryRef = ref(db, 'categories/' + id);
    update(categoryRef, categoryData)
        .then(() => {
            showAlert("Categoría actualizada con éxito.", "success");
            populateCategorySelect();
        })
        .catch((error) => {
            showAlert("Error al actualizar la categoría: " + error.message, "danger");
        });
}

// Evento para añadir/editar categorías
document.getElementById('categoryForm').addEventListener('submit', (e) => {
    e.preventDefault();

    const categoryName = document.getElementById('categoryName').value;
    const editingCategoryId = document.getElementById('categoryForm').getAttribute('data-editing');

    if (editingCategoryId) {
        // Modo de edición: Actualizar la categoría
        updateCategory(editingCategoryId, { name: categoryName });
    } else {
        // Modo de adición: Agregar nueva categoría
        addCategory({ name: categoryName }, (message) => {
            showAlert(message, "success");
        }, (error) => {
            showAlert("Error al agregar la categoría: " + error.message, "danger");
        });
    }

    resetCategoryForm();
    populateCategorySelect();
});

function resetCategoryForm() {
    document.getElementById('categoryForm').reset();
    document.getElementById('categoryForm').removeAttribute('data-editing');
    document.getElementById('submitCategoryBtn').innerText = 'Add Category';
}





//subir la imagen a Storage de Firebase

function uploadImage(file, callback) {
    const storage =  getFirebaseStorage(app);
    const imageRef = storageRef(storage, 'products/' + file.name);
    
    const uploadTask = uploadBytesResumable(imageRef, file);

    // Monitorear el proceso de subida
    uploadTask.on('state_changed', 
        (snapshot) => {
            //Callback para mostrar un progreso de la subida
            const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log('Upload is ' + progress + '% done');
        }, 
        (error) => {
            // En caso de error
            console.error("Error subiendo archivo:", error);
        }, 
        () => {
            // Subida completada: obtener URL de la imagen
            getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
                console.log('File available at', downloadURL);
                callback(downloadURL); // Devuelve la URL
            });
        }
    );
}

//subir la imagen a Storage de Firebase

function uploadImageSlider(file, callback) {
    const storage =  getFirebaseStorage(app);
    const imageRef = storageRef(storage, 'sliders/' + file.name);
    
    const uploadTask = uploadBytesResumable(imageRef, file);

    // Monitorear el proceso de subida
    uploadTask.on('state_changed', 
        (snapshot) => {
            //Callback para mostrar un progreso de la subida
            const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log('Upload is ' + progress + '% done');
        }, 
        (error) => {
            // En caso de error
            console.error("Error subiendo archivo:", error);
        }, 
        () => {
            // Subida completada: obtener URL de la imagen
            getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
                console.log('File available at', downloadURL);
                callback(downloadURL); // Devuelve la URL
            });
        }
    );
}

function showAlert(message, type) {
    const alertElement = document.createElement('div');
    alertElement.className = `alert alert-${type} mt-3`; // `type` puede ser "success" o "danger"
    alertElement.textContent = message;

    // Insertar el alerta en el DOM
    document.getElementById('productForm').before(alertElement);

    // Ocultar el alerta después de 5 segundos
    setTimeout(() => {
        alertElement.remove();
    }, 5000);
}


//Slider

// Mostrar los slides
function displaySlides() {
    const slidesRef = ref(db, 'slides/');
    onValue(slidesRef, (snapshot) => {
        const data = snapshot.val();
        const slidesList = document.getElementById('slidesList');
        slidesList.innerHTML = ''; // Limpiar la lista

        for(let id in data) {
            const slide = data[id];
            let slideItem = `
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    <img src="${slide.image}" alt="Slide Image" width="100" height="auto" class="mr-3 rounded">
                    <div>Type: ${slide.type} - ${slide.name}</div>
                    <div class="button-options">
                        <button data-id="${id}" class="btn btn-warning btn-sm ml-2 edit-slide-btn">Editar</button>
                        <button data-id="${id}" class="btn btn-danger btn-sm ml-2 delete-slide-btn">Eliminar</button>
                    </div>
                </li>`;
            slidesList.innerHTML += slideItem;
        }

        // Event listeners para los botones de eliminar y editar
        document.querySelectorAll('.delete-slide-btn').forEach(btn => {
            btn.addEventListener('click', deleteSlide);
        });
        
        document.querySelectorAll('.edit-slide-btn').forEach(btn => {
            btn.addEventListener('click', editSlide);
        });
    });
}

function deleteSlide(event) {
    const slideId = event.target.getAttribute('data-id');
    const slideRef = ref(db, 'slides/' + slideId);
    set(slideRef, null).then(() => {
        showAlert("Slide eliminado con éxito.", "success");
    }).catch(error => {
        showAlert("Error al eliminar el slide: " + error.message, "danger");
    });
}

let currentEditingSlideId = null; // Guardar el ID del slide que se está editando

function editSlide(event) {
    const slideId = event.target.getAttribute('data-id');
    console.log("Slide ID:", slideId);
    const slideRef = ref(db, 'slides/' + slideId);

    // Obtener los datos del slide desde Firebase y llenar el formulario
    get(slideRef).then((snapshot) => {
        const slide = snapshot.val();
        if (slide) {
            document.getElementById('sliderName').value = slide.name;
            document.getElementById('sliderType').value = slide.type;
            // Si existe una imagen en la base de datos, mostrar la vista previa
            if (slide.image) {
                const imagePreview = document.getElementById('slideImagePreview') || document.createElement('img');
                imagePreview.src = slide.image;
                imagePreview.id = 'slideImagePreview';
                imagePreview.style.maxWidth = '100%';  // Ajustar al contenedor
                imagePreview.style.height = 'auto';
                const previewContainer = document.getElementById('slidePreviewContainer') || document.createElement('div');
                previewContainer.id = 'slidePreviewContainer';
                previewContainer.style.marginTop = '10px';
                document.getElementById('slideImage').parentNode.insertBefore(previewContainer, document.getElementById('slideImage').nextSibling);
                previewContainer.appendChild(imagePreview);
            }

            // Guardar el ID del slide que estamos editando
            currentEditingSlideId = slideId;

            // Cambiar el texto del botón a "Update Slide"
            document.getElementById('submitSlideBtn').textContent = "Update Slide";
        }
    }).catch(error => {
        console.error("Error al obtener datos del slide:", error);
    });
}

//vista previa imagen
document.getElementById('slideImage').addEventListener('change', function(event) {
    const file = event.target.files[0];
    if (file) {
        // Crear una URL para el archivo seleccionado
        const fileReader = new FileReader();
        fileReader.onload = function(e) {
            const imagePreview = document.getElementById('slideImagePreview') || document.createElement('img');
            imagePreview.src = e.target.result;
            imagePreview.id = 'slideImagePreview';
            imagePreview.style.maxWidth = '100%'; // Ajustar al contenedor
            imagePreview.style.height = 'auto';
            const previewContainer = document.getElementById('slidePreviewContainer') || document.createElement('div');
            previewContainer.id = 'slidePreviewContainer';
            previewContainer.style.marginTop = '10px';
            document.getElementById('slideImage').parentNode.insertBefore(previewContainer, document.getElementById('slideImage').nextSibling);
            if (!document.getElementById('slideImagePreview')) {
                previewContainer.appendChild(imagePreview);
            }
        };
        fileReader.readAsDataURL(file);
    }
});


document.getElementById('sliderForm').addEventListener('submit', (e) => {
    e.preventDefault();
    const name = document.getElementById('sliderName').value;
    const type = document.getElementById('sliderType').value;
    const imageFile = document.getElementById('slideImage').files[0];

    if (currentEditingSlideId) {
        // Estamos en modo edición
        const slideToUpdateRef = ref(db, 'slides/' + currentEditingSlideId);

        if (imageFile) {
            // Si se seleccionó una nueva imagen, cárgala y actualiza el slide
            uploadImageSlider(imageFile, (imageUrl) => {
                update(slideToUpdateRef, { name, type, image: imageUrl }).then(() => {
                    showAlert("Slide actualizado con éxito.", "success");
                    resetSliderForm();
                }).catch((error) => {
                    showAlert("Error al actualizar el slide: " + error.message, "danger");
                });
            });
        } else {
            // Si no se seleccionó una nueva imagen, sólo actualiza el tipo
            update(slideToUpdateRef, { name, type }).then(() => {
                showAlert("Slide actualizado con éxito.", "success");
                resetSliderForm();
            }).catch((error) => {
                showAlert("Error al actualizar el slide: " + error.message, "danger");
            });
        }
    } else {
        // Estamos en modo de agregar un nuevo slide
        if (imageFile) {
            uploadImageSlider(imageFile, (imageUrl) => {
                addSlide({ name, type, image: imageUrl }, (message) => {
                    showAlert(message, "success");
                    resetSliderForm();
                }, (error) => {
                    showAlert("Error al agregar el slide: " + error.message, "danger");
                });
            });
        } else {
            showAlert("Seleccione una imagen para el slide.", "warning");
        }
    }
});

function resetSliderForm() {
    document.getElementById('sliderForm').reset();
    // Restablecer el texto del botón y el ID del slide en edición
    document.getElementById('submitSlideBtn').textContent = "Add Slide";
    currentEditingSlideId = null;
}

function addSlide(slide, onSuccess, onError) {
    const slidesRef = ref(db, 'slides');
    push(slidesRef, slide).then(() => {
        onSuccess("¡Slide agregado con éxito!");
    }).catch((error) => {
        onError(error);
    });
}

// Cuando se cargue el documento, llama a displaySlides
document.addEventListener("DOMContentLoaded", function() {
    displayProducts();
    displaySlides();
});
