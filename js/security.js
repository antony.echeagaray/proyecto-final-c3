// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.0/firebase-app.js";
import { getAuth, signInWithEmailAndPassword, createUserWithEmailAndPassword, onAuthStateChanged, signOut, sendPasswordResetEmail } from "https://www.gstatic.com/firebasejs/10.5.0/firebase-auth.js";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCqfDwCSkJONQuV_lVNqdo46eavVCK0lDc",
    authDomain: "proyecto-final-c3-tienda.firebaseapp.com",
    projectId: "proyecto-final-c3-tienda",
    storageBucket: "proyecto-final-c3-tienda.appspot.com",
    messagingSenderId: "834084720593",
    appId: "1:834084720593:web:ba200d4d639a23e20c9af0"
};

// Initialize Firebase
initializeApp(firebaseConfig);
const auth = getAuth();

onAuthStateChanged(auth, user => {
  if (!user) {
    // No está autenticado, redirige al usuario a la página de inicio de sesión
    window.location.href = "/html/login-form.html";
  }
});