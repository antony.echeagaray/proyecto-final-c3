// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.0/firebase-app.js";
import { getAuth, signInWithEmailAndPassword, createUserWithEmailAndPassword, onAuthStateChanged, signOut, sendPasswordResetEmail } from "https://www.gstatic.com/firebasejs/10.5.0/firebase-auth.js";
import { GoogleAuthProvider, signInWithPopup } from "https://www.gstatic.com/firebasejs/10.5.0/firebase-auth.js";
import { getDatabase, ref, push, onValue, set } from "https://www.gstatic.com/firebasejs/10.5.0/firebase-database.js";



// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCqfDwCSkJONQuV_lVNqdo46eavVCK0lDc",
    authDomain: "proyecto-final-c3-tienda.firebaseapp.com",
    projectId: "proyecto-final-c3-tienda",
    storageBucket: "proyecto-final-c3-tienda.appspot.com",
    messagingSenderId: "834084720593",
    appId: "1:834084720593:web:ba200d4d639a23e20c9af0"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth();

// Función autoinvocada para el formulario de inicio de sesión
(function() {
    const loginForm = document.getElementById("loginForm");
    let signOutTimer;

    const resetSignOutTimer = () => {
        if (signOutTimer) clearTimeout(signOutTimer);
        signOutTimer = setTimeout(() => {
            // Cierra la sesión de Firebase
            signOut(auth).then(() => {
                // Redirige al usuario a la página de inicio de sesión u otra página relevante
                window.location.href = '/html/login-form.html';
                alert('Su sesión ha expirado por inactividad.');
            }).catch((error) => {
                console.error('Error al cerrar la sesión', error);
            });
        }, 600000); // 600000 ms = 10 minutos
    };

    // Reinicia el temporizador cada vez que haya actividad
    document.addEventListener('mousemove', resetSignOutTimer);
    document.addEventListener('keypress', resetSignOutTimer);

    if (loginForm) {
        loginForm.addEventListener("submit", function(e) {
            e.preventDefault();
            var userEmail = document.getElementById("userEmail").value;
            var userPassword = document.getElementById("userPassword").value;

            signInWithEmailAndPassword(auth, userEmail, userPassword)
                .then((userCredential) => {
                    // Usuario ha iniciado sesión correctamente
                    resetSignOutTimer(); // Inicia el temporizador de cierre de sesión
                    alert("Inicio de sesión exitoso!");
                    window.location.href = '/html/admin-panel.html';
                })
                .catch((error) => {
                    // Error al iniciar sesión
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    alert("Error: " + errorMessage);
                });
        });
    }
})();

// Función autoinvocada para el formulario de registro
(function() {
    const registerForm = document.getElementById("registerForm");
    if (registerForm) {
        registerForm.addEventListener("submit", function(e) {
            e.preventDefault();
            var registerEmail = document.getElementById("registerEmail").value;
            var registerPassword = document.getElementById("registerPassword").value;

            createUserWithEmailAndPassword(auth, registerEmail, registerPassword)
                .then((userCredential) => {
                    // Usuario registrado con éxito
                    var user = userCredential.user;
                    alert("¡Registro exitoso!");
                    window.location.href ='/html/admin-panel.html';
                })
                .catch((error) => {
                    // Error en el registro
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    alert("Error: " + errorMessage);
                });
        });
    }
})();

//Funcion autoinvocada para la recuperacion de contraseña
(function() {
    document.addEventListener("DOMContentLoaded", function() {
        const passwordResetForm = document.getElementById("passwordResetForm");
        
        if (passwordResetForm) {
            passwordResetForm.addEventListener("submit", function(e) {
                e.preventDefault();

                const userEmail = document.getElementById("resetEmail").value;

                sendPasswordResetEmail(auth, userEmail)
                    .then(() => {
                        alert("¡Correo electrónico de reinicio enviado!");
                        window.location.href ='/html/login-form.html';
                    })
                    .catch((error) => {
                        var errorCode = error.code;
                        var errorMessage = error.message;
                        alert("Error: " + errorMessage);
                    });
            });
        }
    });
})();

//Funcion autoinvocada para iniciar sesión con Google

(function() {
    let signOutTimer;

    const resetSignOutTimer = () => {
        if (signOutTimer) clearTimeout(signOutTimer);
        signOutTimer = setTimeout(() => {
            signOut(auth).then(() => {
                window.location.href = '/html/login-form.html';
                alert('Su sesión ha expirado por inactividad.');
            }).catch((error) => {
                console.error('Error al cerrar la sesión', error);
            });
        }, 600000); // 600000 ms = 10 minutos
    };

    // Reinicia el temporizador cada vez que haya actividad
    document.addEventListener('mousemove', resetSignOutTimer);
    document.addEventListener('keypress', resetSignOutTimer);

    document.addEventListener("DOMContentLoaded", function() {
        const loginWithGoogleBtn = document.getElementById("loginWithGoogle");
        
        if (loginWithGoogleBtn) {
            loginWithGoogleBtn.addEventListener("click", function(e) {
                e.preventDefault();

                const provider = new GoogleAuthProvider();

                signInWithPopup(auth, provider)
                    .then((result) => {
                        // El token de acceso se puede acceder mediante `result.credential.accessToken`
                        const user = result.user;
                        resetSignOutTimer(); // Inicia o reinicia el temporizador de cierre de sesión
                        alert("Inicio de sesión con Google exitoso!");
                        window.location.href = '/html/admin-panel.html';
                    })
                    .catch((error) => {
                        var errorCode = error.code;
                        var errorMessage = error.message;
                        // El correo electrónico de la cuenta del usuario utilizado por el usuario para iniciar sesión.
                        var email = error.email;
                        // La instancia AuthCredential que se usó.
                        var credential = error.credential;
                        alert("Error: " + errorMessage);
                    });
            });
        }
    });
})();


//Ocultar / Mostrar Los enlaces de login o Admin
document.addEventListener("DOMContentLoaded", function() {
    onAuthStateChanged(auth, (user) => {
        const loginLink = document.getElementById("login");
        const adminDropdown = document.getElementById('adminDropdown');
    
        if (user) {
            // Si el usuario ha iniciado sesión
            loginLink.classList.add('d-none');          // Oculta el enlace "Login"
            adminDropdown.classList.remove('d-none');   // Muestra el menú desplegable "Admin"
        } else {
            // Si el usuario no ha iniciado sesión
            loginLink.classList.remove('d-none');       // Muestra el enlace "Login"
            adminDropdown.classList.add('d-none');      // Oculta el menú desplegable "Admin"
        }
    });
});

//Redireccionar al usuario al home al cerrar sesión
document.getElementById("logout").addEventListener('click', function(e) {
    e.preventDefault();
    signOut(auth).then(() => {
        // Sesión cerrada
        window.location.href = '/index.html'; // Redirige al usuario a la página principal
    }).catch((error) => {
        console.error('Error cerrando sesión:', error);
    });
});

//añadir el favicon a todas las paginas

function addFavicon() {
    const head = document.head || document.getElementsByTagName('head')[0];
    const link = document.createElement('link');
    
    link.rel = 'icon';
    link.type = 'image/x-icon';
    link.href = '/img/favicon.png'; // Cambia esto a la ruta de tu favicon

    // Verifica si ya existe un favicon en la página
    const existingFavicon = document.querySelector("link[rel='icon']");
    if (existingFavicon) {
        head.removeChild(existingFavicon);
    }

    head.appendChild(link);
}

// Llama a la función cuando se cargue el documento
document.addEventListener('DOMContentLoaded', addFavicon);
