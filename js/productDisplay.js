import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.0/firebase-app.js";
import { getDatabase, ref, push, onValue, set, update } from "https://www.gstatic.com/firebasejs/10.5.0/firebase-database.js";



// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCqfDwCSkJONQuV_lVNqdo46eavVCK0lDc",
    authDomain: "proyecto-final-c3-tienda.firebaseapp.com",
    projectId: "proyecto-final-c3-tienda",
    storageBucket: "proyecto-final-c3-tienda.appspot.com",
    messagingSenderId: "834084720593",
    appId: "1:834084720593:web:ba200d4d639a23e20c9af0"
};

const app = initializeApp(firebaseConfig); // Configuración de Firebase
const db = getDatabase(app);



function displayProductCards() {
    const db = getDatabase(); // Asegúrate de que esta línea se ajusta a cómo accedes a Firebase en tu frontend.
    const productsRef = ref(db, 'products/');

    onValue(productsRef, (snapshot) => {
        const data = snapshot.val();
        const productsContainer = document.getElementById('productsContainer');
        productsContainer.innerHTML = ''; // Limpiar el contenedor

        for(let id in data) {
            const product = data[id];
            if (product.active && product.category ==='Gamer'){
                let productCard = `
                <div class="col-sm-6 col-xs-12 col-md-3 col-xl-3">
                    <div class="card shadow mb-5 bg-body rounded border-0 text-center product-card">
                        <img src="${product.image}" alt="${product.name}" class="card-img-top">
                        <div class="card-body d-grid ">
                            <h4 class="card-title text-center text-truncate ">${product.name}</h4>
                            <h6 class="card-title text-center text-truncate ">${product.category}</h6>
                            <p class="card-text text-center text-truncate ">${product.extract}</p>
                            <h5 class="text-center price">$${product.price}.00</h5>
                            <a href="" class="btn btn-primary text-center cart">Add to Cart</a>
                        </div>
                    </div>
                </div>`;
            productsContainer.innerHTML += productCard;
            }
        }
        
    });
}


function displayProductCardsHeadphones() {
    const db = getDatabase(); // Asegúrate de que esta línea se ajusta a cómo accedes a Firebase en tu frontend.
    const productsRef = ref(db, 'products/');

    onValue(productsRef, (snapshot) => {
        const data = snapshot.val();
        const productsContainer = document.getElementById('productsContainer-headphones');
        productsContainer.innerHTML = ''; // Limpiar el contenedor

        for(let id in data) {
            const product = data[id];
            if (product.active && product.category ==='Headphones'){
                let productCard = `
                <div class="col-sm-6 col-xs-12 col-md-3 col-xl-3">
                    <div class="card shadow mb-5 bg-body rounded border-0 text-center product-card">
                        <img src="${product.image}" alt="${product.name}" class="card-img-top">
                        <div class="card-body d-grid ">
                            <h4 class="card-title text-center text-truncate ">${product.name}</h4>
                            <h6 class="card-title text-center text-truncate ">${product.category}</h6>
                            <p class="card-text text-center text-truncate ">${product.extract}</p>
                            <h5 class="text-center price">$${product.price}.00</h5>
                            <a href="" class="btn btn-primary text-center cart">Add to Cart</a>
                        </div>
                    </div>
                </div>`;
            productsContainer.innerHTML += productCard;
            }
        }
        
    });
}

function displayProductCardsComponents() {
    const db = getDatabase(); // Asegúrate de que esta línea se ajusta a cómo accedes a Firebase en tu frontend.
    const productsRef = ref(db, 'products/');

    onValue(productsRef, (snapshot) => {
        const data = snapshot.val();
        const productsContainer = document.getElementById('productsContainer-components');
        productsContainer.innerHTML = ''; // Limpiar el contenedor

        for(let id in data) {
            const product = data[id];
            if (product.active && product.category ==='Components'){
                let productCard = `
                <div class="col-sm-6 col-xs-12 col-md-3 col-xl-3">
                    <div class="card shadow mb-5 bg-body rounded border-0 text-center product-card">
                        <img src="${product.image}" alt="${product.name}" class="card-img-top">
                        <div class="card-body d-grid ">
                            <h4 class="card-title text-center text-truncate ">${product.name}</h4>
                            <h6 class="card-title text-center text-truncate ">${product.category}</h6>
                            <p class="card-text text-center text-truncate ">${product.extract}</p>
                            <h5 class="text-center price">$${product.price}.00</h5>
                            <a href="" class="btn btn-primary text-center cart">Add to Cart</a>
                        </div>
                    </div>
                </div>`;
            productsContainer.innerHTML += productCard;
            }
        }
        
    });
}

function displaySlides() {
    const slidesRef = ref(db, 'slides/');

    onValue(slidesRef, (snapshot) => {
        const data = snapshot.val();
        const desktopCarouselInner = document.querySelector('#carouselExampleInterval .carousel-inner');
        const mobileCarouselInner = document.querySelector('#carouselmmobile .carousel-inner');
        
        desktopCarouselInner.innerHTML = ''; 
        mobileCarouselInner.innerHTML = ''; 

        let hasActiveDesktopSlide = false;
        let hasActiveMobileSlide = false;

        for(let id in data) {
            const slide = data[id];
            if (slide.type === "desktop" && slide.active) hasActiveDesktopSlide = true;
            if (slide.type === "mobile" && slide.active) hasActiveMobileSlide = true;
            
            let slideItem = `
                <div class="carousel-item ${slide.active ? 'active' : ''}" data-bs-interval="${slide.interval}">
                    <img src="${slide.image}" class="d-block w-100" alt="...">
                </div>`;

            if (slide.type === "desktop") {
                desktopCarouselInner.innerHTML += slideItem;
            } else if (slide.type === "mobile") {
                mobileCarouselInner.innerHTML += slideItem;
            }
        }

        // Si no hay un slide activo, establecer el primer slide como activo
        if (!hasActiveDesktopSlide && desktopCarouselInner.children.length > 0) {
            desktopCarouselInner.children[0].classList.add('active');
        }
        
        if (!hasActiveMobileSlide && mobileCarouselInner.children.length > 0) {
            mobileCarouselInner.children[0].classList.add('active');
        }
    });
}


// Llamar a la función cuando se cargue el DOM:
document.addEventListener("DOMContentLoaded", function() {
    displayProductCards();
    displayProductCardsHeadphones();
    displayProductCardsComponents();
    displaySlides();
});
